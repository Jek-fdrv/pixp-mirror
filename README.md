PixP
====

####Pixel perfect script for create the html pages.####
See all info **<a href="http://jek-fdrv.16mb.com/pixp">here</a>**.



##If site not work here the git version of info.##

<pre>\<script src="http://jek-fdrv.16mb.com/pixp/pixp.js" type="text/javascript">\</script></pre>
<code>1</code> **Сopy the code and then put in tags '\<head>' or before '\</body>' on your site.**<br>
<code>2</code> **Load your 'pp-bg.jpg'/png in any domain foldes. Example: '/images/'**<br>
<code>3</code> **Select folder name and extension in dialog. Example: '/wp-content/themes/any/images/' - .jpg** <br>
*If u dont see dialog window - press: <code>Shift + E.</code>*

>##  Short cuts:
<ul>
<li><code>Alt + Q</code> switch show/hide.</li>
<li><code>Alt + W</code> switch transparency.</li>
<li><code>Alt + E</code> + transparency.</li>
<li><code>lt + D</code> - transparency.</li>
</ul>

>>### More short cuts:
<ul>
<li><code>Druging</code> Click, hold and move.</li>
<li><code>Shift + Q</code> Default position. </li>
<li><code>Shift + W/A/S/D</code> Sensitive position preference.</li>
<li><code>Shift + E</code> Toggle dialog.</li>
<li><code>Alt + C</code> Switch 'Hover mode'.</li>
</ul>

>>>#### More layouts:
<ul>
<li><code>Alt + 1-9</code> Upload ur images and name pp-bg1.jpg, pp-bg9.jpg... Now u can switch them.</li>
<li><code>Alt + X</code> Show default image - pp-bg.jpg/png.</li>
<li><code>Alt + Z</code> For toggle overflow.</li>
</ul>

###Hints:###
<table>
    <tr>
If u have pp-bg.jpg u must use pp-bg1.jpg (same extension). U cant use .png if ur defaul img is pp-bg.jpg. <br />
U can type url of image, but if it not named 'pp-bg' u cant use layouts.<br />
Script use cookie, so if you reload page you will see all settings have been saved.<br />
Hover mode - when you use inspect mode script will hide the image while you are working with Elements.<br />
If you see cropped image press: <code>Alt + Z</code><br />
 </tr>
</table>
